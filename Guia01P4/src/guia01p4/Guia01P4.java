/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia01p4;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Fredy diaz
 */
public class Guia01P4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
      
        int numero;
        
        System.out.println("Menu de ejercicios");
        System.out.println("1. Cálculo del Radio y Área de un Círculo");
        System.out.println("2. Cálculo de Logitud de una Circunferencia");
        System.out.println("3. Pedir 2 Números y decir si son iguales o no");
        System.out.println("4. Pedir 1 Número e indicar si es Positivo o Pegativo");
        System.out.println("5. Pedir 2 Números y decir si uno es Múltiplo del otro");
        System.out.println("6. Pedir 2 números y decir cual es el Mayor o si son Iguales");
        System.out.println("7. Pedir 3 Números y pueda mostrarlos ordenados de Mayor a Menor");
        System.out.println("8. Pedir 1 Número entre 0 y 9.999 y pueda decir cuántas cifras tiene");
        System.out.println("9. Solicite una palabra y pueda determinar si se escribe igual al revés");
        System.out.println("10. Pedir 2 Fechas y mostrar el Número de días que hay de diferencia");
        
         do{
        numero=sc.nextInt();
        
        
        switch(numero){
           
            case 1:
            double A, R;
            
            System.out.println("Introduzca el Radio de un Cículo:");
            R=sc.nextDouble();
            
            A=Math.PI*(R*R);
              System.out.println("El Área del Círculo de Radio:"+R+ "es:"+A );
                
                break;
            
            case 2:
                double r, l;
                
                System.out.println("Ingrese el radio de la Circunferencia");
                r=sc.nextDouble();
                
                l=2*Math.PI*r;
                System.out.println("La longitud de la Circunferencia de Radio:"+r+ " es:"+l);
                
                
                break;
            
            case 3:
                int num1, num2;
                
                 System.out.println("Ingrese el primer número");
                 num1=sc.nextInt();
                 
                  System.out.println("Ingrese el segundo numero");
                  num2=sc.nextInt();
                  
                  if(num1==num2){
                       System.out.println("Los numeros son iguales");    
                  }else{
                       System.out.println("Los numeros son diferentes");
                  }
                
                break;
             
            case 4:
                int n;
                
                 System.out.println("Ingrese algun numero: ");
                 n=sc.nextInt();
                 
                 if(n<0){
                      System.out.println("El numero es Negativo");    
                 }else{
                      System.out.println("El numero es Positivo");
                 }
                 
                break;
             
            case 5:
                   System.out.println("Ingrese el primer número");
                   int a = sc.nextInt();
                      System.out.println("Ingrese el segundo número");
                   int b = sc.nextInt();
                   
                   if(a % b == 0){
                       System.out.println("El numero: "+b+ " es multiplo de: "+a);   
                   }else{
                       System.out.println("El numero: "+b+ " no es multiplo de: "+a);
                   }
                
                break;
              
            case 6:
               int n1, n2;
               
                  System.out.println("por favor ingrese un numero: ");
                  n1=sc.nextInt();
                  System.out.println("por favor ingrese otro numero");
                  n2=sc.nextInt();
                  
                  if(n1==n2){
                  System.out.println("Los numeros son iguales");  
                  
                  }
                  else if(n1>n2){
                  System.out.println(n1+ " es mayor que "+n2);
                  }
                  else if(n1<n2){
                  System.out.println(n2+ " es mayor que "+n1);
                      
                  }
                
                break;
                
            case 7:
                int nu1, nu2, nu3;
                
                System.out.println("Ingrese el primer numero");
                nu1 = sc.nextInt();
                System.out.println("Ingrese el segundo numero");
                nu2 = sc.nextInt();
               System.out.println("Ingrese el tercer numero");
                nu3 = sc.nextInt();
                
                if((nu1>nu2) && (nu2>nu3)){
                System.out.println( "Su orden es: "+nu1+" - "+nu2+" - "+nu3);   
                }
                else if((nu1>nu3) && (nu3>nu2)){
                System.out.println( "Su orden es: "+nu1+" - "+nu3+" - "+nu2);    
                }
                else if((nu2>nu1) && (nu1>nu3)){
                System.out.println( "Su orden es: "+nu2+" - "+nu1+" - "+nu3);   
                }
                else if((nu2>nu3 && (nu3>nu1))){
                System.out.println( "Su orden es: "+nu2+" - "+nu3+" - "+nu1); 
                }
                else if((nu3>nu1) && (nu1>nu2)){
                System.out.println( "Su orden es: "+nu3+" - "+nu1+" - "+nu2);     
                }
                else{
                System.out.println( "Su orden es: "+nu3+" - "+nu2+" - "+nu1); 
                }
                break;
            
            case 8:
                int num;
             

            System.out.print("Introduzca un número entre 0 y 9999: ");
            num=sc.nextInt();
            
              if (num<10) {
            System.out.println ("El numero tiene 1 cifra");
        }
              
         else if (num<100) {
            System.out.println ("El numero es de 2 cifras"); 
         }

        else if (num<1000) {
            System.out.println ("El numero es de 3 cifras");
        }

         else if(num<10000){

            System.out.println ("El numero tiene 4 cifras");
        }
                break;
           
            case 9:
                String palabra,alreves="";
                System.out.println ("Escriba una palabra");
                palabra=sc.next();
                for(int i=palabra.length()-1; i >= 0; i--)
                alreves += palabra.charAt(i);
                
                if(palabra.equals(alreves)){
                    System.out.println ("La palabra  se escribe igual alreves");  
                }
                else{
                    System.out.println ("La palabra  no se escribe igual alreves");   
                }
                break;
                
            case 10:
                int dia1,mes1,año1;
                int dia2,mes2,año2;
                int total_dias;
                
            System.out.println ("Fecha 1:");
            System.out.print("Introduzca el día: ");
            dia1=sc.nextInt();
            System.out.print("Introduzca el mes: ");
            mes1=sc.nextInt();
            System.out.print("Introduzca el año: ");
            año1=sc.nextInt();
            System.out.println ("Fecha 2:");
            System.out.print("Introduzca el día: ");
            dia2=sc.nextInt();
            System.out.print("Introduzca el mes: ");
            mes2=sc.nextInt();
            System.out.print("Introduzca el año: ");
            año2=sc.nextInt();
            
 // suponemos que las fecha introducidas son correctas
 // convertimos las dos fechas a días y calculamos la diferencia
 total_dias = dia2-dia1 + 30*(mes2-mes1)+365*(año2-año1);
 System.out.println ("Los días de diferencia es: " + total_dias);
                
                break;
                
            
        }
        
        
        }while(numero>0 || numero<11);  
        
    }
    
}
